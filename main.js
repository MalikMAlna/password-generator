// Global DOM Element Variables
const resultEl = document.getElementById("result");
const lengthEl = document.getElementById("length");
const uppercaseEl = document.getElementById("uppercase");
const lowercaseEl = document.getElementById("lowercase");
const numbersEl = document.getElementById("numbers");
const symbolsEl = document.getElementById("symbols");
const generateEl = document.getElementById("generate");
const clipboardEl = document.getElementById("clipboard");

// Generator Functions
const getRandomUpper = () => {
  return String.fromCharCode(Math.floor(Math.random() * 26) + 65);
};

const getRandomLower = () => {
  return String.fromCharCode(Math.floor(Math.random() * 26) + 97);
};

const getRandomNumber = () => {
  return String.fromCharCode(Math.floor(Math.random() * 10) + 48);
};

const getRandomSymbol = () => {
  const symbols = "!@#$%^&*(){}[]=<>/,.-?";
  return symbols[Math.floor(Math.random() * symbols.length)];
};

// Random Function Object
const randomFunc = {
  upper: getRandomUpper,
  lower: getRandomLower,
  number: getRandomNumber,
  symbol: getRandomSymbol,
};

// Generate Password Event Listener
generateEl.addEventListener("click", () => {
  const length = parseInt(lengthEl.value);
  const hasUpper = uppercaseEl.checked;
  const hasLower = lowercaseEl.checked;
  const hasNumber = numbersEl.checked;
  const hasSymbol = symbolsEl.checked;

  resultEl.innerText = generatePassword(
    length,
    hasUpper,
    hasLower,
    hasNumber,
    hasSymbol
  );
});

// Copy Password To Clipboard
clipboardEl.addEventListener("click", () => {
  const textarea = document.createElement("textarea");
  const password = resultEl.innerText;

  if (!password) {
    return;
  }

  textarea.value = password;
  document.body.appendChild(textarea);
  textarea.select();
  document.execCommand("copy");
  textarea.remove();
  alert("Password copied to clipboard.");
});

// Generate Password Function
const generatePassword = (length, upper, lower, number, symbol) => {
  // Initialize pw var
  let generatedPassword = "";

  // Filter out unchecked types
  const typesCount = upper + lower + number + symbol;
  console.log("typesCount: ", typesCount);

  const typesArr = [{ upper }, { lower }, { number }, { symbol }].filter(
    (type) => Object.values(type)[0]
  );
  console.log("typesArr: ", typesArr);

  if (typesCount === 0) {
    return "";
  }

  // loop over length and call gen func for each type
  for (let i = 0; i < length; i += typesCount) {
    typesArr.forEach((type) => {
      const funcName = Object.keys(type)[0];
      console.log("funcName: ", funcName);

      // Add final password to pass var and return it
      generatedPassword += randomFunc[funcName]();
    });
  }
  const finalPassword = generatedPassword.slice(0, length);

  return finalPassword;
};

// Character Codes - http://www.net-comber.com/charset.html
